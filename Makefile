# change to your local directories!
PD_APP_DIR = /usr/lib/pd
GEM_DIR = /usr/include/Gem
Eigen_Dir = /usr/include/eigen3

CPPFLAGS = -I$(GEM_DIR) -I$(Eigen_Dir) -I/usr/include/pd
UNAME := $(shell uname -s)
 CPPFLAGS += -I
 CXXFLAGS = -g -O2 -fPIC -freg-struct-return -Os -falign-loops=32 -falign-functions=32 -falign-jumps=32 -funroll-loops -ffast-math -mmmx
 LDFLAGS = -shared -rdynamic
 LIBS = 
 EXTENSION = pd_linux
 USER_EXTERNALS=$HOME/.local/lib/pd/extra

.SUFFIXES = $(EXTENSION)

SOURCES = pix_depth_spat_tracking

all:
	g++ $(CPPFLAGS) $(CXXFLAGS) -o $(SOURCES).o -c $(SOURCES).cpp
	g++ -o $(SOURCES).$(EXTENSION) $(LDFLAGS) $(SOURCES).o $(LIBS)
	rm -fr ./*.o

clean:
	rm -f $(SOURCES)*.o
	rm -f $(SOURCES)*.$(EXTENSION)

distro: clean all
	rm *.o
